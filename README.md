# Climax Statistiques App (UI)

Application de visualisation de donnees et de statistiques sur les clients de la societe Climax a travers des fichiers CSV, JSON, XML.

## Fonctionnalites

- Telechargement d'un fichier de donnees
- Visualation des donnees contenues dans un fichier

## Technologies utilisees

- Langages : HTML, CSS, JavaScript
- Librairie : React Js

## Prerequis

- Node Js v20.10.0
- npm v10.2.5

## Installation et configuration

- Cloner le depot git
```git clone https://gitlab.com/climax2/climax-ui.git```
- Acceder au dossier de l'application
- Installer les dependences : ```npm install```
- Lancer le serveur : ```npm start```

## Utilisation
- Cliquer sur le bouton `Telecharger` pour selectionner un fichier de donnees a sauvegarder et lire
- Cliquer sur le bouton `Charger` pour choisir un fichier deja sauvegarde pour le lire

