import React from 'react'

const Heading = ({title, options}) => {
  return (
    <div className="d-sm-flex justify-content-between align-items-start">
        <div>
            <h4 className="card-title card-title-dash">{title}</h4>
        </div>
        {options && 
            <div>
                <span className="btn btn-success btn-lg text-white mb-0 me-0" data-bs-toggle="modal" data-bs-target="#upload_data"><i className="mdi mdi-cloud-upload"></i>Telecharger</span>
                <span className="btn btn-success btn-lg text-white mb-0 me-0" data-bs-toggle="modal" data-bs-target="#download_data"><i className="mdi mdi-cloud-download"></i>Charger</span>
            </div>
        }
    </div>
  )
}

export default Heading