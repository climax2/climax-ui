import React, { useContext } from 'react'
import api from '../../api/api'
import StatistiquesContext from '../../context/StatistiquesContext'

const UploadData = ({setFiles, files}) => {
    const {setIdFile} = useContext(StatistiquesContext)

    const handleSubmit = (e) => {
        e.preventDefault()
        
        const data = {
            file : document.getElementById("data").files[0]
        }
        api.post("api/file/", data, {
            headers : {
                "Content-Type" : "multipart/form-data"
            }
        }).then((res) => {
            setIdFile(res.data.idFile)
            setFiles([...files, res.data])
        }).catch((err) => {
            console.log(err)
        }) 
    }
  return (
    <div className="modal fade" id="upload_data" aria-hidden="true" aria-labelledby="upload_data" tabIndex="-1">
        <div className="modal-dialog modal-dialog-centered">
            <div className="modal-content">
                <div className="modal-header">
                    <h5 className="modal-title" id="upload_data">Telecharger des donnees</h5>
                    <button type="button" className="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <form onSubmit={e => handleSubmit(e)} >
                    <div className="modal-body">
                        <label htmlFor="" className="form-label">Selectionnez le fichier</label>
                        <input type="file" name="" id="data" className="form-control form-control-lg"/>
                    </div>
                    <div className="modal-footer">
                        <button className="btn btn-success btn-lg text-white mb-0 me-0">Ajouter</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
  )
}

export default UploadData