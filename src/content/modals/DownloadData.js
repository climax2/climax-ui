import React, { useContext, useEffect } from 'react'
import StatistiquesContext from '../../context/StatistiquesContext'
import api from '../../api/api'

const DownloadData = ({files, setFiles}) => {
    const {setIdFile} = useContext(StatistiquesContext)

    useEffect(() => {
        api.get("api/file/").then((res) => {
            setFiles(res.data)
        }).catch((err) => {
            console.log(err)
        })
    }, [setFiles])

  return (
    <div className="modal fade" id="download_data" aria-hidden="true" aria-labelledby="download_data" tabIndex="-1">
        <div className="modal-dialog modal-dialog-centered">
            <div className="modal-content">
                <div className="modal-header">
                    <h5 className="modal-title" id="download_data">Charger des donnees</h5>
                    <button type="button" className="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div className="modal-body">
                    <p className='mb-3'>Liste des fichiers de donnees deja telecharges</p>
                    <ul className="bullet-line-list">
                        {files.map((file) => <li onClick={() => setIdFile(file.idFile)}>{file.nameFile}</li>)}
                    </ul>
                </div>
                <div className="modal-footer">
                    <button className="btn btn-danger btn-lg text-white mb-0 me-0" data-bs-dismiss="modal">Fermer</button>
                </div>
            </div>
        </div>
    </div>
  )
}

export default DownloadData