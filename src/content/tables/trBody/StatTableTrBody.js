import React from 'react'

const StatTableTrBody = ({stat}) => {
  return (
    <tr>
        <td>
          <h6>{stat.intituleProfession}</h6>
        </td>
        <td>
          <h6>{stat.moyenneSalaire}</h6>
        </td>
    </tr>
  )
}

export default StatTableTrBody