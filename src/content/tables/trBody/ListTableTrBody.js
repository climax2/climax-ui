import React from 'react'

const ListTableTrBody = ({client}) => {
  return (
    <tr>
        <td>
            <h6>{client.nomClient}</h6>
            <p>{client.prenomClient}</p>
        </td>
        <td>
            <h6>{client.ageClient}</h6>
        </td>
        <td>
            <h6>{client.professionClient}</h6>
        </td>
        <td>
            <h6>{client.salaireClient}</h6>
        </td>
    </tr>
  )
}

export default ListTableTrBody