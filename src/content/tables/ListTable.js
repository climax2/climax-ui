import React, { useContext } from 'react'
import ListTableTrHead from './trHead/ListTableTrHead'
import ListTableTrBody from './trBody/ListTableTrBody'
import StatistiquesContext from '../../context/StatistiquesContext'

const ListTable = () => {
  const {clients} = useContext(StatistiquesContext)
  return (
    <div className="table-responsive  mt-1 border-bottom mb-5">
        <table className="table select-table">
            <thead>
                <ListTableTrHead/>
            </thead>
            <tbody>
                {clients.map((client) => <ListTableTrBody client={client} />)}
            </tbody>
        </table>
    </div>
  )
}

export default ListTable