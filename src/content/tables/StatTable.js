import React, { useContext } from 'react'
import StatTableTrHead from './trHead/StatTableTrHead'
import StatTableTrBody from './trBody/StatTableTrBody'
import StatistiquesContext from '../../context/StatistiquesContext'

const StatTable = () => {
  const {stats} = useContext(StatistiquesContext)
  return (
    <div className="table-responsive  mt-1">
        <table className="table select-table">
            <thead>
                <StatTableTrHead/>
            </thead>
            <tbody>
                {stats.map((stat) => <StatTableTrBody stat={stat}/>)}
            </tbody>
        </table>
    </div>
  )
}

export default StatTable