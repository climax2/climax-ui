import React, { useState } from 'react'
import Heading from './heading/Heading'
import ListTable from './tables/ListTable'
import StatTable from './tables/StatTable'
import UploadData from './modals/UploadData'
import DownloadData from './modals/DownloadData'

const Content = () => {
    const [files, setFiles] = useState([])
  return (
    <div className="row">
        <div className="col-sm-12">
            <div className="home-tab">
                <div className="d-sm-flex align-items-center justify-content-between border-bottom"></div>
                
                <div className="tab-content tab-content-basic">
                    <div className="tab-pane fade show active" id="overview" role="tabpanel" aria-labelledby="overview">
                        <div className="row flex-grow">
                            <div className="col-12 grid-margin stretch-card">
                                <div className="card card-rounded">
                                    <div className="card-body">
                                        <Heading title={"Donnees"} options={true}/>

                                        <ListTable />

                                        <Heading title={"Statistiques"} />

                                        <StatTable />

                                        <UploadData files={files} setFiles={setFiles}/>
                                        <DownloadData files={files} setFiles={setFiles}/>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div> 
            </div>
        </div>
    </div>
  )
}

export default Content