import React from 'react'
import logo from "../assets/images/logo.svg"

const Nav = () => {
  return (
    <nav className="navbar default-layout col-lg-12 col-12 p-0 fixed-top d-flex align-items-top flex-row">
      <div className="text-center navbar-brand-wrapper d-flex align-items-center justify-content-start">
        <div>
          <span className='navbar-brand brand-logo'><img src={logo} alt="logo" /></span>
          <a className="navbar-brand brand-logo-mini" href="/">
            <img src={logo} alt="logo" />
          </a>
        </div>
      </div>
      <div className="navbar-menu-wrapper d-flex align-items-top"> 
        <ul className="navbar-nav">
          <li className="nav-item font-weight-semibold ms-0">
            <h1 className="welcome-text">Bienvenue</h1>
          </li>
        </ul>
      </div>
    </nav>
  )
}

export default Nav