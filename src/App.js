import Content from "./content/Content";
import { StatistiquesProvider } from "./context/StatistiquesContext";
import Footer from "./core/Footer";
import Nav from "./core/Nav";

function App() {
  return (
    <div className="container-scroller">
      <Nav/>
      <div className="container-fluid page-body-wrapper">
        <div className="main-panel">
          <div className="content-wrapper">
            <StatistiquesProvider><Content /></StatistiquesProvider>
          </div>

          <Footer/>
        </div>
      </div>
    </div>
  );
}

export default App;
