import { createContext, useEffect, useState } from "react";
import api from "../api/api";

const StatistiquesContext = createContext({})

export const StatistiquesProvider = ({children}) => {
    const [idFile, setIdFile] = useState(null)
    const [clients, setClients] = useState([])
    const [stats, setStats] = useState([])

    useEffect(() => {
        idFile && api.get(`api/file/${idFile}`).then((res) => {
            setClients(res.data.clients)
            setStats(res.data.statistiques)
        }).catch((err) => {
            console.log(err)
        })
    }, [idFile, setClients, setStats])
    return (
        <StatistiquesContext.Provider
            value={{
                setIdFile, clients, stats 
            }}
        >
            {children}
        </StatistiquesContext.Provider>
    )
}

export default StatistiquesContext